data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*-hvm-*"]
  }
}


resource "aws_instance" "ec2_server" {
  count                  = 1
  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.ec2_server_sg.id]
  key_name               = "ec2-key-pem"

  tags = {
    Name      = "server-${count.index}"
    CreatedBy = "Terraform"
  }
}

