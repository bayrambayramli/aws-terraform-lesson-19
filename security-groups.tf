
resource "aws_security_group" "ec2_server_sg" {
  name        = var.ec2_server_sg_name
  description = "Allow traffic from MyIP"
  vpc_id      = var.vpc_id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.ec2_server_sg_name
  }

  lifecycle {
    create_before_destroy = true
  }
}
