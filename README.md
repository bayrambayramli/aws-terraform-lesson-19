# Lesson 19: Terraform and GitLab
Ingress Academy AWS & Terraform Course

This terraform module stores state file in Amazon S3 bucket and uses DynamoDB table for state locking.  
More info: https://developer.hashicorp.com/terraform/language/settings/backends/s3

**Requirements:**  
S3 Bucket: delta-tfstate-s3-backend  
Versioning: Enabled

DynamoDB Table: terraform-state-lock  
Partition key: LockID, type: String  

