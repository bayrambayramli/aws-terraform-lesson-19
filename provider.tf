# AWS PROVIDER CONFIGURATION

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  backend "s3" {
    bucket         = "delta-tfstate-s3-backend"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-state-lock"
  }
}

provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Company    = "IngressAcademy"
      CourseName = "AWS-and-Terraform"
      Lesson     = var.lesson
      ManagedBy  = "Terraform"
      Platform   = "GitLab"
    }
  }
}


